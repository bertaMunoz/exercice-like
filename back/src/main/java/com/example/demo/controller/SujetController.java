package com.example.demo.controller;

import com.example.demo.models.Message;
import com.example.demo.models.Sujet;
import com.example.demo.services.impl.MessageServiceImpl;
import com.example.demo.services.impl.SujetServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin
@RequestMapping("/forumExo/sujets")
public class SujetController {
    @Autowired
    private SujetServiceImpl service;

    @GetMapping
    public List<Sujet> findAll() {
        return  this.service.findAll();
    }

    @GetMapping("{id}")
    public Sujet findById(@PathVariable String id) {
        return this.service.findById(id);
    }

    @PostMapping()
    public Sujet create(@RequestBody Sujet sujet ) {
        return this.service.create(sujet);
    }

    @PutMapping()
    public Sujet update(@RequestBody Sujet sujet) {
        return this.service.update(sujet);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        this.service.deleteById(id);
    }

    @PostMapping("{id}/messages")
    public Sujet addMessage(@RequestBody Message message, @PathVariable String id){
       return  this.service.addMess(id, message);
    }
}
