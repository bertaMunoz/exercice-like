package com.example.demo.repositories;

import com.example.demo.models.Sujet;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SujetRepository extends MongoRepository<Sujet, String> {

}
