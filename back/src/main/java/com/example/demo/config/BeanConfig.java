package com.example.demo.config;

import com.example.demo.repositories.MessageRepository;
import com.example.demo.repositories.SujetRepository;
import com.example.demo.services.impl.MessageServiceImpl;
import com.example.demo.services.impl.SujetServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    MessageServiceImpl messageService(MessageRepository repository) {
        return new MessageServiceImpl(repository);
    }

    @Bean
    SujetServiceImpl sujetService(SujetRepository repossitory, MessageServiceImpl messageService) {
        return new SujetServiceImpl(repossitory, messageService);
    }
}
