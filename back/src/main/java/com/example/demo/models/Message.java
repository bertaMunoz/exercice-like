package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Message {

    @Id
    private String id;
    private String content;
    @CreatedDate
    private LocalDate createdAt;
    @LastModifiedDate
    private LocalDate updateAt;
}

