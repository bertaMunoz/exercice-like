package com.example.demo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Sujet {



    @Id
    private String id;
    private String title;
    private String likes;
    @DBRef
    private List<Message> messages;
    @CreatedDate
    private LocalDate createdAt;
    @LastModifiedDate
    private LocalDate updateAt;

    public void addMessage(Message message) {
        this.messages.add(message);
    }

}
