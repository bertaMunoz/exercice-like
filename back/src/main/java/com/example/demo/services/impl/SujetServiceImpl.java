package com.example.demo.services.impl;

import com.example.demo.models.Message;
import com.example.demo.models.Sujet;
import com.example.demo.repositories.MessageRepository;
import com.example.demo.repositories.SujetRepository;
import com.example.demo.services.GenericService;

import java.time.LocalDate;
import java.util.List;

public class SujetServiceImpl implements GenericService<Sujet> {


    private final SujetRepository repository;
    private final MessageServiceImpl messervice;

    public SujetServiceImpl(SujetRepository repository, MessageServiceImpl messervice) {
        this.repository = repository;
        this.messervice = messervice;
    }

    @Override
    public List<Sujet> findAll() {
        return this.repository.findAll();
    }

    @Override
    public Sujet findById(String id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Sujet create(Sujet entity) {
        entity.setCreatedAt(LocalDate.now());
        return this.repository.save(entity);
    }

    @Override
    public Sujet update(Sujet entity) {
        entity.setUpdateAt(LocalDate.now());

        return this.repository.save(entity);
    }

    @Override
    public void deleteById(String id) {
        this.repository.deleteById(id);
    }

    public Sujet addMess(String id, Message message) {
        Sujet sujet = this.repository.findById(id).get();
        message = this.messervice.create(message);
        sujet.addMessage(message);
        return this.repository.save(sujet);
    }
}
