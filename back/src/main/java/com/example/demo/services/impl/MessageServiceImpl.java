package com.example.demo.services.impl;

import com.example.demo.models.Message;
import com.example.demo.models.Sujet;
import com.example.demo.repositories.MessageRepository;
import com.example.demo.services.GenericService;

import java.time.LocalDate;
import java.util.List;

public class MessageServiceImpl implements GenericService<Message> {

    private final MessageRepository repository;

    public MessageServiceImpl(MessageRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Message> findAll() {
        return this.repository.findAll();
    }

    @Override
    public Message findById(String id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Message create(Message entity) {
        entity.setCreatedAt(LocalDate.now());
        return this.repository.save(entity);
    }

    @Override
    public Message update(Message entity) {
        entity.setUpdateAt(LocalDate.now());
        return this.repository.save(entity);
    }

    @Override
    public void deleteById(String id) {
         this.repository.deleteById(id);
    }

   
}
